﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class FpsCustom : MonoBehaviour {
	
	public FirstPersonController fps;
	public StamineBar stamineBar;
	public float stamine = 1;
	public float power = 0f;
	public float maxPowerBar;
	public float input = 0.0f;

	void Start () {
		maxPowerBar = fps.m_RunSpeed;
		power = maxPowerBar;
	}

	void Update () {
		StamineUpdate ();
	}


	void StamineUpdate () {		

		if (!fps.m_IsWalking)
			input = Mathf.Abs (Input.GetAxis ("Vertical"));
		else
			input = 0.0f;

		if (power >= 0.0f && power <= maxPowerBar) 
			power = Mathf.MoveTowards(power, 0.05f, input * Time.deltaTime);

		if (input <= 0f) 
			power = Mathf.MoveTowards (power, maxPowerBar, Time.deltaTime);

		fps.m_RunSpeed = (power < 0.1f) ? fps.m_WalkSpeed : maxPowerBar;

		stamineBar.ChangeScale(new Vector3 (power, 1, 1));

	}
}
