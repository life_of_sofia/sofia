﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact : MonoBehaviour {
	public string tag = "interactive";
	public GameObject interactPanel;
	public float interval = 1f;
	public InteractiveUI interactiveUI;

	ActiveDeactive toggler;
	RaycastHit hit;
	Transform currentHit;
	bool hasHit = false;
	Ray ray;
	
	void Start () {
		StartCoroutine (RayCastUpdate());
	}

	IEnumerator RayCastUpdate () {
		while (true) {
			ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out hit)) {
				if (hit.collider != null) {
					HandleHit (hit.transform);
					yield return new WaitForSeconds (interval);
				}
			} 
			else if (hasHit) {
				DetachHit ();
			}
			yield return new WaitForSeconds (interval);
		}
	}

	void HandleHit (Transform hit) {
		if (hit.transform.tag == tag) {
			hasHit = true;
			interactPanel.SetActive (true);
			int id = hit.GetComponent<InteractiveElement> ().id;
			interactiveUI.ChangeMesh (id);
		} 
		else if (hasHit) {
			DetachHit ();
		}
	}
		
	public void DetachHit () {
		interactPanel.SetActive (false);
		interactiveUI.StopAnim ();
		hasHit = false;
	}

}
