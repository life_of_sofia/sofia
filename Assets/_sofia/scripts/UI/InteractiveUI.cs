﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveUI : MonoBehaviour {

	public Mesh[] meshs;

	float y = 0;
	Coroutine animRoutine;

	public void ChangeMesh(int id) { 
		transform.GetComponent<MeshFilter>().mesh = meshs [id];
		animRoutine = StartCoroutine ("Animate");
	}
	public void StopAnim() {
		transform.GetComponent<MeshFilter> ().mesh = null;
		StopCoroutine("Animate");
	}
	public IEnumerator Animate () {
		while (true) {
			transform.localRotation = Quaternion.Euler (new Vector3 (0, y, 0));
			y = (y > 360) ? 0 : y += 15;
			yield return new WaitForSeconds (0.2f);	
		}
	}

}
