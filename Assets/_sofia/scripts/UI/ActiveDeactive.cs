﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveDeactive : MonoBehaviour {

	public void Active (GameObject gObject) {
		gObject.SetActive (true);
	}

	void Active (GameObject gObject, float time) {
		StartCoroutine (sleep (time));
		gObject.SetActive (true);
	}

	public void Deactive (GameObject gObject) {
		gObject.SetActive (false);
	}

	void Deactive (GameObject gObject, float time) {
		StartCoroutine (sleep (time));
		gObject.SetActive (true);
	}

	private IEnumerator sleep (float time) {
		yield return new WaitForSeconds (time);
	}

}
